#include "Shader.h"
#include <glad/glad.h>
#include "Window.h"
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
namespace Hydrogen {
	//Static shaders!
	Shader* Shader::S_TEST_SHADER = nullptr; 
	Shader* Shader::_2D_SHADER = nullptr;



	Shader::Shader(std::string vertexSource, std::string fragmentSource)
	{
		this->vertexSource = vertexSource;  //Vertex source code of the newly created shader
		this->fragmentSource = fragmentSource; //Fragment source code of the newly created shader
		int vShader = Window::compileShader(vertexSource.c_str(), VERTEX_SHADER); //Compiles the source and return a shader ID
		int fShader = Window::compileShader(fragmentSource.c_str(), FRAGMENT_SHADER);  //Compiles the source and return a shader ID
		this->Id = Window::createShaderProgram(vShader, fShader); //Creates a shader program and returns its ID
	}

	Shader::~Shader()
	{
	}

	unsigned int Shader::getId()
	{
		return this->Id;
	}

	//Reads shader source code from a file
	std::string Shader::getShaderFromSource(std::string path) {
		std::string code;
		std::ifstream shaderFile;
		shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		try {
			shaderFile.open(path);
			std::stringstream shaderStream;

			shaderStream << shaderFile.rdbuf();
			shaderFile.close();
			code = shaderStream.str();
		}
		catch (std::ifstream::failure e) {
			std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
		}
		return code;
	}


	//Initializes the static shaders
	void Shader::initShaders()
	{
		Shader::S_TEST_SHADER = new Shader(getShaderFromSource("testVertex.glsl"), getShaderFromSource("testFragment.glsl"));
		Shader::_2D_SHADER = new Shader(getShaderFromSource("2DVertex.glsl"), getShaderFromSource("2DFragment.glsl"));
	}

	//Functions to load uniforms into shaders.

	void Shader::setBool(unsigned int shaderId, const std::string & name, bool value) 
	{
		glUniform1i(glGetUniformLocation(shaderId, name.c_str()), (int)value);
	}

	void Shader::setInt(unsigned int shaderId, const std::string & name, int value) 
	{
		glUniform1i(glGetUniformLocation(shaderId, name.c_str()), value);
	}

	void Shader::setFloat(unsigned int shaderId, const std::string & name, float value) 
	{
		glUniform1f(glGetUniformLocation(shaderId, name.c_str()), value);
	}

	void Shader::setTransform(unsigned int shaderId,  glm::mat4 value)
	{
		unsigned int loc = glGetUniformLocation(shaderId, "transform");
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::setView(unsigned int shaderId, glm::mat4 value)
	{
		unsigned int loc = glGetUniformLocation(shaderId, "view");
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
	}

	void Shader::setProjection(unsigned int shaderId, glm::mat4 value)
	{
		unsigned int loc = glGetUniformLocation(shaderId, "projection");
		glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(value));
	}


	
}