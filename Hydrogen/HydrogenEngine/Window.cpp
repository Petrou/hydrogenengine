#include <glad/glad.h>
#include "Window.h"
#include <iostream>
#include "Shader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Timer.h"

namespace Hydrogen {
	namespace Window {
		Hydrogen::Application* Window::application = nullptr;//Pointer to the application to run;
		std::vector<unsigned int> VAOS; //List of vaos for cleaning purposes;
		std::vector<unsigned int> VBOS; //List of vbos for cleaning purposes;
		std::vector<int> shaderPrograms; //List of shader programs for cleaning purposes;
		std::vector<int> Textures; //List of Textures for cleaning purposes;
		void createWindow(int width_, int height_, std::string title, Hydrogen::Application* app) //creates a window

		{
			
			width = width_ * 1.0f; //Saves the window width in a global variable.
			height = height_ * 1.0f; //Saves the window height in a global variable.
			//projection = glm::perspective(glm::radians(70.0f), width / height, 0.01f, 1000.0f); //Sets the projection matrix for rendering.
			projection2D = glm::ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f);   //The standard projection matrix
			projection3D = glm::perspective(glm::radians(90.0f), (float)width / (float)height, 0.1f, 100.0f);
			viewMatrix = glm::translate(viewMatrix, glm::vec3(0.0f, 0.0f, -3.0f));
			
			//init glfw with core opengl 3.3
			glfwInit();
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
			glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

			//create a window with the given variables.
			GLFWwindow* window = glfwCreateWindow(width_, height_, title.c_str(), NULL, NULL);
			if (window == NULL)
			{
				std::cout << "Failed to create GLFW window" << std::endl;
				glfwTerminate();
				exit(-1);
			}
			glfwMakeContextCurrent(window); //make the context current
			glfwSwapInterval(0); //Removes fps limit
			glfwSetFramebufferSizeCallback(window, framebuffer_size_callback); //set screen resize callback
			mainWindow = window; //assign the created window as main window.


			//init GLAD
			if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
			{
				std::cout << "Failed to initialize GLAD" << std::endl;
				exit(-1);
			}



			application = app; //assign app to run;
			app->start(); //run app
			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); //sets rendermode to wireframe;
			//start engine loop
			Sprite::vaoID = createVAO(Sprite::vertices, Sprite::indices, Sprite::textureCoords);

		
			limitFPS = 1.0f / glfwGetVideoMode(glfwGetPrimaryMonitor())->refreshRate;
			glEnable(GL_DEPTH_TEST); //Enables depth testing
			while (!glfwWindowShouldClose(window))
			{
				//Timer* calc = new Timer();
				////////////////////////////////////////////////////////////
				//Counts fps and delta time
				nowTime = glfwGetTime();
				deltaTime += (nowTime - lastTime) / limitFPS;
				lastTime = nowTime;
				///////////////////////////////////////////////////////////
				glClearColor(0.2f, 0.3f, 0.3f, 1.0f); //clear opengl color
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); //clear opengl color buffer
			
				while (deltaTime >= 1.0) {
					application->update();  // - Update function
					updates++;
					deltaTime--;
				}
				application->render();
				glfwSwapBuffers(window); //swap the primary buffer
				glfwPollEvents(); //poll for glfw events
				////////////////////////////////////////////////////////////
				//Counts fps and delta time
				frames++;
				if (glfwGetTime() - timer > 1.0) {
					timer++;
					std::cout << "FPS: " << frames << " Updates:" << updates << std::endl;
					updates = 0, frames = 0;
				}
				////////////////////////////////////////////////////////////
				//delete calc;
			}
			cleanUp(); //clean engine 
		}
		//calls when window resizes
		void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
			glViewport(0, 0, width, height); //set opengl viewport to actual window dimensions
			projection2D = glm::ortho(0.0f, (float)width, (float)height, 0.0f, -1.0f, 1.0f);   //The standard projection matrix
			projection3D = glm::perspective(glm::radians(90.0f), (float)width / (float)height, 0.1f, 100.0f);
		}



		

		//clean engine
		void cleanUp() {
			for (unsigned int i = 0; i < VAOS.size(); i++) {
				glDeleteBuffers(1, &VAOS.at(i));
			}
			for (unsigned int i = 0; i < VBOS.size(); i++) {
				glDeleteBuffers(1, &VBOS.at(i));
			}
			for (unsigned int i = 0; i < shaderPrograms.size(); i++) {
				glDeleteProgram(shaderPrograms.at(i));
			}
			for (unsigned int i = 0; i < Textures.size(); i++) {
				unsigned int arr[100];
				std::copy(Textures.begin(), Textures.end(), arr);
				glDeleteTextures(Textures.size() , arr);
			}
			application->stop(); //stop the application
			glfwTerminate(); //terminate glfw
			std::cin.get();
			exit(0);
		}

		//compiles the given shader
		int Window::compileShader(const char* source, int shadertype) {
			int shader = glCreateShader(shadertype); //creates shader	

			glShaderSource(shader, 1, &source, NULL); //links to cont char* source
			glCompileShader(shader); //compiles

			int  success;
			char infoLog[512];
			glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

			if (!success)
			{
				glGetShaderInfoLog(shader, 512, NULL, infoLog);
				std::cout << "ERROR::SHADER::COMPILATION_FAILED\n" << infoLog << std::endl; //prints if compiling failed;
			}

			return shader;
		}
		//creates a shader program
		int Window::createShaderProgram(int vertex, int fragment)
		{
			int program = glCreateProgram(); //creates the program
			glAttachShader(program, vertex); //attaches the vertex shader
			glAttachShader(program, fragment); //attaches the fragment shader 
			glLinkProgram(program); //links the program together
			int  success;
			char infoLog[512];
			glGetProgramiv(program, GL_LINK_STATUS, &success);
			if (!success) {
				glGetProgramInfoLog(program, 512, NULL, infoLog);
				std::cout << "ERROR::SHADERPROGRAM::LINKING_FAILED\n" << infoLog << std::endl; //prints if linking failed;
			}
			//cleans individual shaders from the memory
			glDeleteShader(vertex);
			glDeleteShader(fragment);
			shaderPrograms.push_back(program); //adds program to list for clean up on engine termination;
			return program; //return the program id;
		}

		//creates a buffer
		unsigned int Window::createVAO(std::vector<float> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords)
		{

			unsigned int VBO, VAO, EBO, TEX;
			glGenVertexArrays(1, &VAO);
			glBindVertexArray(VAO);

			glGenBuffers(1, &VBO);
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW); //buffers the vertices to bound VAO&VBO;


			glGenBuffers(1, &EBO);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(int), indices.data(), GL_STATIC_DRAW);//buffers the indices to bound VAO&VBO;

			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0); //enables vertices attribute for shader (on layout 0)
			glEnableVertexAttribArray(0);

			glGenBuffers(1, &TEX);
			glBindBuffer(GL_ARRAY_BUFFER, TEX);
			glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(float), texCoords.data(), GL_STATIC_DRAW); //buffers the coords to bound VAO&VBO;
			
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)0); //enables texture attribute for shader (on layout 1)
			glEnableVertexAttribArray(1);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
			//adds created buffers to list for cleanup on engine termination;
			VBOS.push_back(VBO);
			VBOS.push_back(EBO);
			VBOS.push_back(TEX);
			VAOS.push_back(VAO);

			return VAO;
		}

		unsigned int loadTexture(const char* file) //Loads a texture.
		{
			unsigned int texture;
			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);
			int width, height, nrChannels;
			// set the texture wrapping/filtering options (on the currently bound texture object)
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			stbi_set_flip_vertically_on_load(true);
			
			unsigned char *data = stbi_load(file, &width, &height, &nrChannels, 0);
			if (data)
			{
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);
			}
			else
			{
				std::cout << "Failed to load texture" << std::endl;
			}

			stbi_image_free(data);
			Textures.push_back(texture); //Stores ID for cleanup
			return texture;
		}

		void screenToContext(float *x, float *y)
		{
			*x = (2 / width)* *x - 1;
			*y = (2 / height)* *y - 1;
		}
		void Window::batchRender(Model* model) {
			modelBatch.push_back(model);
		}

		void Window::render() {
			boundMesh = modelBatch.at(0)->getMesh();
			glBindVertexArray(boundMesh->getVertexCount());
			glActiveTexture(GL_TEXTURE0); //Sets the active texture
			
			
			for (unsigned int i = 0; i < modelBatch.size(); i++) {
				if (modelBatch.at(i)->getMesh() == boundMesh) {

					boundMaterial = modelBatch.at(i)->getMaterial();
					glBindTexture(GL_TEXTURE_2D, boundMaterial->getTexture()); //Binds the texture
					glUseProgram(boundMaterial->getShader());
					Hydrogen::Shader::setView(boundMaterial->getShader(), viewMatrix); //Loads the actual view matrix
					Hydrogen::Shader::setProjection(boundMaterial->getShader(), projection3D); //Loads the actual projection matrix

					for (unsigned int j = 0; j < modelBatch.size(); j++) {
						if (modelBatch.at(j)->getMaterial() == boundMaterial) {
							Hydrogen::Shader::setTransform(modelBatch.at(0)->getMaterial()->getShader(), modelBatch.at(j)->getTransform());
							glDrawElements(GL_TRIANGLES, modelBatch.at(0)->getMesh()->getVertexCount(), GL_UNSIGNED_INT, 0); //renders
							modelBatch.erase(modelBatch.begin()+j);
						}
					}
				}
			}
		}
		
		//renders given 3d model
		void Window::renderModel(Model* model)
		{
			glUseProgram(model->getMaterial()->getShader()); //choses shaderprogram to use
			model->preRender();
			Hydrogen::Shader::setView(model->getMaterial()->getShader(), viewMatrix); //Loads the actual view matrix
			Hydrogen::Shader::setProjection(model->getMaterial()->getShader(), projection3D); //Loads the actual projection matrix
			glBindVertexArray(model->getVaoId()); //binds Vao to render
			glActiveTexture(GL_TEXTURE0); //Sets the active texture
			glBindTexture(GL_TEXTURE_2D, model->getMaterial()->getTexture()); //Binds the texture
			glDrawElements(GL_TRIANGLES, model->getVertexCount() , GL_UNSIGNED_INT, 0); //renders
			glBindVertexArray(0); //unbinds vao for next call;
		}


		//renders given sprite
		void Window::renderSprite(Sprite* sprite)
		{
			glUseProgram(sprite->getMaterial()->getShader()); //choses shaderprogram to use
			sprite->preRender();
			Hydrogen::Shader::setProjection(sprite->getMaterial()->getShader(), projection2D); //Loads the actual projection matrix
			glBindVertexArray(sprite->vaoID); //binds Vao to render
			glActiveTexture(GL_TEXTURE0); //Sets the active texture
			glBindTexture(GL_TEXTURE_2D, sprite->getMaterial()->getTexture()); //Binds the texture
			glDrawElements(GL_TRIANGLES, sprite->vertexCount, GL_UNSIGNED_INT, 0); //renders
			glBindVertexArray(0); //unbinds vao for next call;
		}

		
	}
}

