#include "Timer.h"
#include <iostream>

//measures the time between the constructor and the destructor
namespace Hydrogen {
	std::chrono::time_point<std::chrono::steady_clock> begin;
	Timer::Timer()
	{

		begin = std::chrono::high_resolution_clock::now();
	}


	Timer::~Timer()
	{
		std::chrono::duration<float> duration = std::chrono::high_resolution_clock::now() - begin;
		std::cout << duration.count()*1000.0f << std::endl;
	}
}