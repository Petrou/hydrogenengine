#include "Model.h"

namespace Hydrogen {


	

	Model::Model(Mesh * mesh, Material * material, glm::mat4 transform)
	{
		this->material = material;
		this->mesh = mesh;
		this->transform = transform;
	}

	Model::~Model()
	{
	}
	
	Mesh* Model::getMesh() {
		return mesh;
	}
	Material * Model::getMaterial()
	{
		return material;
	}
	glm::mat4 Model::getTransform()
	{
		return transform;
	}
}