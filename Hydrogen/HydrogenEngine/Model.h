#pragma once
#include "Material.h"
#include "Mesh.h"
#include <glm/glm.hpp>
namespace Hydrogen {
	class Model
	{
	public:
		Model(Mesh* mesh, Material* material, glm::mat4 transform);
		~Model();
		Mesh* mesh;
		Mesh * getMesh();
		Material* getMaterial();
		glm::mat4 getTransform();
	private:
		Mesh* mesh;
		Material* material;
		glm::mat4 transform;
	};


}