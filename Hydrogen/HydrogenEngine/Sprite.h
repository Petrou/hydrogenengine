#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "Material.h"
namespace Hydrogen {
	class Sprite
	{
	public:
		Sprite(float x, float y, float width, float height, Material* matertial);
		~Sprite();

		glm::mat4 transformMatrix; //Model transformation

		void Move(float x, float y, float); //Moves model by translation
		void Scale(glm::vec3 scale); //Scales the model
		void Rotate(float angle, glm::vec3 axis); //Rotates the model on the given axis
		Material* getMaterial();
		static unsigned int vertexCount; //The sprite vertex count
		void Destroy();
		void preRender(); //prepares the sprite to be rendered
		static unsigned int vaoID;
		const static std::vector<float> vertices;
		const static std::vector<unsigned int> indices;
		const static std::vector<float> textureCoords;
	private:
		glm::vec2 position;
		glm::vec2 size;
		Material* mat = nullptr;
		
		

	};
}