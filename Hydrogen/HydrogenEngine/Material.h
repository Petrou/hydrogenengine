#pragma once
#include <string>
namespace Hydrogen {
	class Material
	{
	public:
		Material(unsigned int texture, unsigned int shader, std::string uniforms); //Material constructor
		~Material();
		unsigned int getTexture(); //The texture that applies to this material
		unsigned int getShader(); //The material shader
		void prepare(); //Loads the uniforms into the shader

	private:
		unsigned int texture;
		unsigned int shader;
		std::string uniforms;
	};
}
