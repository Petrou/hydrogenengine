#pragma once

namespace Hydrogen {
	class Application
	{
	public:
		Application();//Application constructor. Leave empty!
		~Application();//Application destructor. Leave empty!
		virtual void start() = 0; //Runs when the application starts;
		virtual void update() = 0; //Runs when the application updates;
		virtual void render() = 0; //Runs when the application renders;
		virtual void stop() = 0; //Runs when the application stops;
	};

}