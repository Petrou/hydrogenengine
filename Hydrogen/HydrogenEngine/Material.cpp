#include "Material.h"
#include <vector>
#include <sstream>
#include <iostream>
#include "Shader.h"
#include "Timer.h"

namespace Hydrogen {

	Material::Material(unsigned int texture, unsigned int shader, std::string uniforms)
	{
		this->texture = texture;
		this->shader = shader;
		this->uniforms = uniforms;
	}


	Material::~Material()
	{
	}

	unsigned int Material::getTexture()
	{
		return texture;
	}

	unsigned int Material::getShader()
	{
		return shader;
	}
	//Loads all uniform values
	void Material::prepare()
	{
		
		std::stringstream test(uniforms);
		std::string segment;
		std::vector<std::string> seglist;

		while (std::getline(test, segment, ' '))
		{
			seglist.push_back(segment);
		}
		int index = 0;
		std::string type;
		std::string name;
		for (unsigned int i = 0; i < seglist.size(); i++) {
			std::string s = seglist.at(i);

			if (index == 0) {

				if (s == "bool") {
					index++;
					type = s;
				}
				else if (s == "int") {
					index++;
					type = s;
				}
				else if (s == "float") {
					index++;
					type = s;
				}

			}
			else if (index == 1) {
				name = s;
				index++;
			}
			else {
				if (type == "bool") {
					index = 0;
					if (s == "false") {
						Shader::setBool(shader, name, true);
					}
					else {
						Shader::setBool(shader, name, false);
					}
				}
				else if (type == "int") {
					index = 0;
					std::istringstream iss(s);
					int integer = 0;
					iss >> integer;
					Shader::setInt(shader, name, integer);
				}
				else if (type == "float") {
					index = 0;
					std::istringstream iss(s);
					float integer = 0;
					iss >> integer;
					Shader::setFloat(shader, name, integer);
				}
			}
		}
	}
}