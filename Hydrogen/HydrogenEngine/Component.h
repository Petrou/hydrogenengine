#pragma once
class Component
{
public:
	Component();
	~Component();
	virtual void init(unsigned int parentId);
	unsigned int getParentId() {
		return this->parentId;
	}
private:
	unsigned int parentId;
};

