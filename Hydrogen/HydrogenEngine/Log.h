#pragma once
#include <string>
#include <iostream>
#include "Window.h"

namespace Hydrogen {
	namespace Log {

		enum WarningLevel : char {Message=0, Warning=1, Error=2};
		void log(std::string message, WarningLevel level) {
			if (DEBUG_LEVEL >= level) {
				std::cout << message << std::endl;
			}

		}
		void error(std::string message) {
			log(message, WarningLevel::Error);
		}
		void warning(std::string message) {
			log(message, WarningLevel::Warning);
		}
		
		void message(std::string message) {
			log(message, WarningLevel::Message);
		}
		
		

	}
}