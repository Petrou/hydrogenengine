#include "Sprite.h"
#include <glm/gtc/matrix_transform.hpp>
#include "Window.h"
#include "Shader.h"
namespace Hydrogen {
	const std::vector<float> Sprite::textureCoords = { 1.0f, 1.0f,
												1.0f, 0.0f,
												0.0f, 0.0f,
												0.0f, 1.0f
	};

	const std::vector<float> Sprite::vertices = { -1.0f, 1.0f, 0.0f,//v0
											-1.0f, -1.0f, 0.0f,//v1.0
											 1.0f, -1.0f, 0.0f,//v2
											 1.0f, 1.0f, 0.0f,//v3
	};

	const std::vector<unsigned int> Sprite::indices = {
											0,1,3,//top left triangle (v0, v1, v3)
											3,1,2//bottom right triangle (v3, v1, v2)
	};

	unsigned int Sprite::vaoID = 0;

	Sprite::Sprite(float x, float y, float width, float height, Material* material)
	{
		mat = material; //The main material for this sprite
		position = glm::vec2(x, y); //sets the coordinates
		size = glm::vec2(width/2, height/2); //sets the size
		transformMatrix = glm::translate(transformMatrix, glm::vec3(position, 0.0f));
		transformMatrix = glm::scale(transformMatrix, glm::vec3(size, 1.0f));
		Rotate(glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f)); //Mirrors the sprite to normal rotation
	}

	unsigned int Sprite::vertexCount = Sprite::vertices.size();

	Sprite::~Sprite()
	{
	}


	void Sprite::Move(float x, float y, float z) //Moves the Sprite by translation
	{
		transformMatrix = glm::translate(transformMatrix, glm::vec3(x, y, z));
	}
	void Sprite::Scale(glm::vec3 scale) //Scales the Sprite
	{
		transformMatrix = glm::scale(transformMatrix, scale);

	}
	void Sprite::Rotate(float angle, glm::vec3 axis) //Rotates Sprites on the given axis
	{
		transformMatrix = glm::rotate(transformMatrix, angle, axis);
	}
	Material * Sprite::getMaterial()
	{
		return mat;
	}



	void Sprite::Destroy() //Used to clean the model from memory
	{
		delete(this); //Deletes the data at the model pointer
	}
	void Sprite::preRender()
	{
		
		Shader::setTransform(mat->getShader(), transformMatrix);
		mat->prepare();
	}
}