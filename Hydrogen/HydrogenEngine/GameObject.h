#pragma once
#include <vector>
#include "Component.h"
class GameObject
{
public:
	~GameObject();
	GameObject(unsigned int ID = 0, std::vector<Component*> components=std::vector<Component*>());
	void addComponent(Component* toAdd);
	template <class T> void removeComponent();
	template <class T>void getComponent(T component);
	void start();
	void update();
	void stop();
	unsigned int getId() {
		return this->ID;
	}

private:
	unsigned int ID;
	unsigned int generateId();
	std::vector<Component*> components = std::vector<Component*>();
	
};

template<class T>
inline void GameObject::removeComponent()
{
	for (unsigned int i = 0; i < this->components.size(); i++) {
		if (std::is_same<T, Component>::value) {
			delete components.at(i);
			return;
		}
	}
}

template<class T>
inline void GameObject::getComponent(T component)
{
	for (unsigned int i = 0; i < this->components.size(); i++) {
		if (std::is_same<this->components.at(i), component>::value) {
			return components.at(i);
		}
	}
}
