#pragma once
class Mesh
{
public:
	Mesh(unsigned int VAO, int vertexCount);
	~Mesh();
	unsigned int getVAO() {
		return VAO;
	}
	int  getVertexCount() {
		return vertexCount;
	}

private:
	unsigned int VAO;
	int vertexCount;

};

