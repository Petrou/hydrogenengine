#include "GameObject.h"





void GameObject::addComponent(Component* toAdd)
{
	this->components.push_back(toAdd);
}



void GameObject::start()
{
	for (unsigned int i = 0; i < this->components.size(); i++) {
		if (this->components.at(i)!=nullptr) {
			this->components.at(i)->init(this->ID);
		}
	}
}

void GameObject::update()
{
}

void GameObject::stop()
{
}


GameObject::~GameObject()
{
}

unsigned int GameObject::generateId()
{
	
	return 0;
}

GameObject::GameObject(unsigned int ID, std::vector<Component*> components)
{
	if (ID==0) {
		this->ID=generateId();
	}
	if (components.size() != 0) {
		std::copy(components.data(), components.data()+components.size(), this->components.begin());
	}
}
