#pragma once

#include <GLFW/glfw3.h>
#include <string>
#include "Application.h"
#include <vector>
#include "Model.h"
#include <chrono>
#include "Sprite.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
namespace Hydrogen {
#define VERTEX_SHADER  0x8B31 //Refrence to opengl/GL_VERTEX_SHADER
#define FRAGMENT_SHADER  0x8B30 //Refrence to opengl/GL_FRAGMENT_SHADER
#define DEBUG_LEVEL 2
	namespace Window {
		static GLFWwindow* mainWindow; //The application main window
		void createWindow(int width_, int height_, std::string title, Application* app); //Creates glfwWindow with given width, height and window title. Whenr ready it also inits glad and the glViewport.
		void framebuffer_size_callback(GLFWwindow* window, int width, int height); //Function to run when window is resized
		
		void cleanUp();
		void batchRender(Model * model);
		void render();
		//Cleans the engine
		void renderModel(Model* model); //Renders the buffer
		void renderSprite(Sprite* sprite);

		int compileShader(const char* source, int shadertype); //Compiles a shader
		int createShaderProgram(int vertex, int fragment); //Links shaders into shaderprogram
		static float width, height;
		unsigned int createVAO(std::vector<float> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords); //Creates a VAO
		unsigned int loadTexture(const char* file); //Loads a texture
		void screenToContext(float *x, float *y);
		
		
		static glm::mat4 viewMatrix = glm::mat4(1.0f);
		// note that we're translating the scene in the reverse direction of where we want to move
		
		static glm::mat4 projection2D = glm::ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f);   //The standard projection matrix
		static glm::mat4 projection3D = glm::perspective(glm::radians(90.0f), (float)1 / (float)1, 0.1f, 100.0f);
		
		extern Application* application; //The application to run

		static double limitFPS = 1.0 / 60.0;

		static double lastTime = glfwGetTime(), timer = lastTime;
		static double deltaTime = 0, nowTime = 0;
		static int frames = 0, updates = 0;

		static std::vector<Model*> modelBatch;
		static Mesh* boundMesh = nullptr;
		static Material* boundMaterial = nullptr;
	}
}