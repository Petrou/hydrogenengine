#pragma once
#include "GameComponent.h"
#include <vector>
class MeshComponent :
	public GameComponent
{
public:
	MeshComponent(std::vector<float> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords);
	~MeshComponent();
	unsigned int getVaoId(); //The model vao id
	unsigned int getVertexCount(); //The model shader id
	void Destroy();	//Used to clean the model from memory
	void preRender(); //Ran before rendering

private:

	unsigned int vao = 0; //The model vao id
	unsigned int vertexCount = 0; //The amount of vertices the model has
};

