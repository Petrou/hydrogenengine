#pragma once
#include "../Component.h"
class GameComponent :
	public Component
{
public:
	GameComponent();
	~GameComponent();
	void init(unsigned int parentId) override;
	virtual void update() = 0;
};

