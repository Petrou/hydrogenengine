#pragma once
#include "StaticComponent.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
class TransformComponent :
	public StaticComponent
{
public:
	TransformComponent(glm::mat4 transform = glm::mat4(1.0f));
	~TransformComponent();
	glm::mat4 transformMatrix; //Model transformation

	void Move(float x, float y, float); //Moves model by translation
	void Scale(glm::vec3 scale); //Scales the model
	void Rotate(float angle, glm::vec3 axis); //Rotates the model on the given axis
};
 
