#include "MeshComponent.h"
#include "../Window.h"



MeshComponent::MeshComponent(std::vector<float> vertices, std::vector<unsigned int> indices, std::vector<float> texCoords)
{
	vao = Hydrogen::Window::createVAO(vertices, indices, texCoords); //Creates a vao with the given data buffers.
	vertexCount = (unsigned int)vertices.size(); //The amount of vertices the model has.
}


MeshComponent::~MeshComponent()
{
}

unsigned int MeshComponent::getVaoId()  //The model vao id
{
	return vao;
}



unsigned int MeshComponent::getVertexCount() //The amount of vertices the model has.
{
	return vertexCount;
}

void MeshComponent::Destroy()
{
	delete(this); //Deletes the data at the model pointer
}

void MeshComponent::preRender()
{
	//Hydrogen::Shader::setTransform(mat->getShader(), transformMatrix); //Sets the transform uniform in the shader
	//mat->prepare();
}
