#include "TransformComponent.h"



TransformComponent::TransformComponent(glm::mat4 transform)
{
	transformMatrix = transform;
}


TransformComponent::~TransformComponent()
{
}

void TransformComponent::Move(float x, float y, float z) //Moves the model by translation
{
	transformMatrix = glm::translate(transformMatrix, glm::vec3(x, y, z));
}
void TransformComponent::Scale(glm::vec3 scale) //Scales the model
{
	transformMatrix = glm::scale(transformMatrix, scale);

}
void TransformComponent::Rotate(float angle, glm::vec3 axis) //Rotates models on the given axis
{
	transformMatrix = glm::rotate(transformMatrix, angle, axis);
}
