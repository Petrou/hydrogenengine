#pragma once
#include "../Component.h"
class StaticComponent :
	public Component
{
public:
	StaticComponent();
	~StaticComponent();
	void init(unsigned int parentId) override;
};

