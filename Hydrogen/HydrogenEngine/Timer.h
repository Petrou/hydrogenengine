#pragma once

#include <chrono>
namespace Hydrogen {
	//measures the time between the constructor and the destructor
	class Timer
	{
	public:
		Timer();
		~Timer();
	};

}