#pragma once
#include <fstream>
#include <sstream>
#include <string>
#include <glm/glm.hpp>
namespace Hydrogen {
	class Shader
	{
	public:
		Shader(std::string vertexSource, std::string fragmentSource); //Shader constructor that takes in the glsl source code.
		~Shader(); //Shader destructor
		unsigned int getId(); //Shaderprogram id

		static Shader* _2D_SHADER; //The test shader program
		static Shader* S_TEST_SHADER; //The test shader program
		

		static std::string getShaderFromSource(std::string path); //Reads shaders source files
		static void initShaders(); //Initializes all the static shader programs
		 
		//Functions to set shader boolean uniforms
		static void setBool(unsigned int shaderId, const std::string &name, bool value) ;
		//Functions to set shader integer uniforms
		static void setInt(unsigned int shaderId, const std::string &name, int value) ;
		//Functions to set shader float uniforms
		static void setFloat(unsigned int shaderId, const std::string &name, float value) ;
		//Functions to set shader transform matrix
		static void setTransform(unsigned int shaderId, glm::mat4 value);
		//Functions to set shader view matrix
		static void setView(unsigned int shaderId, glm::mat4 value);
		//Functions to set shader projection matrix
		static void setProjection(unsigned int shaderId, glm::mat4 value);
	private:
		unsigned int Id; //Shaderprogram id
		std::string vertexSource; //Source code of the vertex shader
		std::string fragmentSource; //Source code of the fragment shader
	};

}

