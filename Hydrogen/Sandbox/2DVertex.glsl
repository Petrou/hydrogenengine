#version 330 core
	
	layout (location = 0) in vec3 aPos;
	layout (location = 1) in vec2 aTexCoord;

	uniform mat4 transform;
	uniform mat4 projection;


	out vec2 TexCoord;


	void main()
	{
	 gl_Position = projection * transform * vec4(aPos.xy, 0.0, 1.0);
	  TexCoord = aTexCoord;
	}