#pragma once
#include "Window.h"
#include "Sprite.h"
class Main : public Hydrogen::Application {
public:	
	Main();
	~Main();
	virtual void start() override;
	virtual void update() override;
	virtual void render() override;
	virtual void stop() override;

	Hydrogen::Sprite* sprites[50][50];
	unsigned int program = 0;
	Hydrogen::Model* model = nullptr;
	Hydrogen::Sprite* sprite = nullptr;

	
};
