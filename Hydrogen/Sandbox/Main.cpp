#include "Main.h"
#include <iostream>
#include "Model.h"
#include "Shader.h"
#include "Log.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include "GameObject.h"
#include "components/TransformComponent.h"


int main()
{
	Hydrogen::Window::createWindow(800, 600, "Sandbox", new Main());
}



std::vector<float> vertices = {
			-0.5f,0.5f,-0.5f,
				-0.5f,-0.5f,-0.5f,
				0.5f,-0.5f,-0.5f,
				0.5f,0.5f,-0.5f,

				-0.5f,0.5f,0.5f,
				-0.5f,-0.5f,0.5f,
				0.5f,-0.5f,0.5f,
				0.5f,0.5f,0.5f,

				0.5f,0.5f,-0.5f,
				0.5f,-0.5f,-0.5f,
				0.5f,-0.5f,0.5f,
				0.5f,0.5f,0.5f,

				-0.5f,0.5f,-0.5f,
				-0.5f,-0.5f,-0.5f,
				-0.5f,-0.5f,0.5f,
				-0.5f,0.5f,0.5f,

				-0.5f,0.5f,0.5f,
				-0.5f,0.5f,-0.5f,
				0.5f,0.5f,-0.5f,
				0.5f,0.5f,0.5f,

				-0.5f,-0.5f,0.5f,
				-0.5f,-0.5f,-0.5f,
				0.5f,-0.5f,-0.5f,
				0.5f,-0.5f,0.5f
};

std::vector<float> textureCoords = {
				0,0,
				0,1,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0,
				0,0,
				0,1,
				1,1,
				1,0
};

std::vector<unsigned int> indices = {  // note that we start from 0!
				0,1,3,
				3,1,2,
				4,5,7,
				7,5,6,
				8,9,11,
				11,9,10,
				12,13,15,
				15,13,14,
				16,17,19,
				19,17,18,
				20,21,23,
				23,21,22
};


Main::Main()
{
	
}

Main::~Main()
{
}

void Main::start()
{
	



	Hydrogen::Shader::initShaders(); //Initializes static shaders

	Hydrogen::Material* material2D = new Hydrogen::Material(Hydrogen::Window::loadTexture("test.jpg"), Hydrogen::Shader::_2D_SHADER->getId(), "");
	Hydrogen::Material* material = new Hydrogen::Material(Hydrogen::Window::loadTexture("test.jpg"), Hydrogen::Shader::S_TEST_SHADER->getId(), "");

	GameObject* object = new GameObject();
	TransformComponent* transform = new TransformComponent();
	object->addComponent(transform);
	object->start();



	model = new Hydrogen::Model(vertices, indices, textureCoords, material); //Creates a model
	//sprite = new Hydrogen::Sprite(32, 32, 64, 64, material2D);
	

}

void Main::update()
{
	//model->Rotate(0.05f, glm::vec3(1.0f, 1.0f, 0.0f)); //Rotates the model
	

}

void Main::render() {
	Hydrogen::Window::renderModel(model); //Renders the model
	//Hydrogen::Window::renderSprite(sprite);

	
}

void Main::stop()
{
	model->Destroy(); //Removes the model
	//sprite->Destroy();

	
}
